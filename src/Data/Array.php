<?php

//------------------------------------------------------------------------------
// Array creation --------------------------------------------------------------
//------------------------------------------------------------------------------

$exports['range'] = function ($start) {
  return function ($end) use (&$start) {
    $step = $start > $end ? -1 : 1;
    $result = [];
    $i = $start;
    $n = 0;
    while ($i !== $end) {
      $result[$n++] = $i;
      $i += $step;
    }
    $result[$n] = $i;
    return $result;
  };
};

$exports['replicate'] = function ($count) {
  return function ($value) use (&$count) {
    if ($count < 1) {
      return [];
    }
    return array_pad([], $count, $value);
  };
};

$exports["fromFoldableImpl"] = (function () {
  $emptyList = [];

  $curryCons = function($head) {
    return function ($tail) use (&$head) {
      return ["head" => $head, "tail" => $tail];
    };
  };

  $listToArray = function($list) use (&$emptyList) {
    $result = [];
    $count = 0;
    $xs = $list;
    while ($xs !== $emptyList) {
      $result[$count++] = $xs['head'];
      $xs = $xs['tail'];
    }
    return $result;
  };

  return function ($foldr) use (&$listToArray, &$curryCons) {
    return function ($xs) use (&$listToArray, &$curryCons, &$foldr) {
      return $listToArray($foldr($curryCons)($emptyList)($xs));
    };
  };
})();

//------------------------------------------------------------------------------
// Array size ------------------------------------------------------------------
//------------------------------------------------------------------------------

$exports["length"] = function ($xs) {
  return count($xs);
};

//------------------------------------------------------------------------------
// Extending arrays ------------------------------------------------------------
//------------------------------------------------------------------------------

$exports["cons"] = function ($e) {
  return function ($l) use (&$e) {
    return array_merge([$e], $l);
  };
};

$exports["snoc"] = function ($l) {
  return function ($e) use (&$l) {
    return array_merge($l, [$e]);
  };
};

//------------------------------------------------------------------------------
// Non-indexed reads -----------------------------------------------------------
//------------------------------------------------------------------------------

$exports['uncons\''] = function ($empty) {
  return function ($next) use (&$empty) {
    return function ($xs) use (&$empty, &$next) {
      return empty($xs) ? $empty([]) : $next($xs[0])(array_slice($xs, 1));
    };
  };
};

//------------------------------------------------------------------------------
// Indexed operations ----------------------------------------------------------
//------------------------------------------------------------------------------

$exports["indexImpl"] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($xs) use (&$just, &$nothing) {
      return function ($i) use (&$just, &$nothing, &$xs) {
        return $i < 0 || $i >= count($xs) ? $nothing :  $just($xs[$i]);
      };
    };
  };
};

$exports["findIndexImpl"] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($f) use (&$just, &$nothing) {
      return function ($xs) use (&$just, &$nothing, &$f) {
        for ($i = 0, $l = count($xs); $i < $l; $i++) {
          if ($f($xs[$i])) return $just($i);
        }
        return $nothing;
      };
    };
  };
};

$exports["findLastIndexImpl"] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($f) use (&$just, &$nothing) {
      return function ($xs) use (&$just, &$nothing, &$f) {
        for ($i = count($xs) - 1; $i >= 0; $i--) {
          if ($f($xs[$i])) return $just($i);
        }
        return $nothing;
      };
    };
  };
};

$exports["_insertAt"] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($i) use (&$just, &$nothing) {
      return function ($a) use (&$just, &$nothing, &$i) {
        return function ($l) use (&$just, &$nothing, &$i, &$a) {
          if ($i < 0 || $i > count($l)) return $nothing;
          $l1 = array_slice($array);
          array_splice($l1, $i, 0, [$a]);
          return $just($l1);
        };
      };
    };
  };
};

$exports["_deleteAt"] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($i) use (&$just, &$nothing) {
      return function ($l) use (&$just, &$nothing, &$i) {
        if ($i < 0 || $i >= count($l)) return $nothing;
        $l1 = array_slice($l);
        array_splice($l1, $i, 1);
        return $just($l1);
      };
    };
  };
};

$exports["_updateAt"] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($i) use (&$just, &$nothing) {
      return function ($a) use (&$just, &$nothing) {
        return function ($l) use (&$just, &$nothing, &$a){
          if ($i < 0 || $i >= count($l)) return $nothing;
          $l1 = array_slice($l);
          $l1[$i] = $a;
          return $just($l1);
        };
      };
    };
  };
};

//------------------------------------------------------------------------------
// Transformations -------------------------------------------------------------
//------------------------------------------------------------------------------

$exports["reverse"] = function ($l) {
  return array_reverse($l) ;
};

$exports["concat"] = function ($xss) {
  return array_merge(... $xss);
};

$exports["filter"] = function ($f) {
  return function ($xs) use (&$f) {
    return array_filter($xs, $f);
  };
};

$exports["partition"] = function ($f) {
  return function ($xs) use (&$f) {
    $yes = [];
    $no  = [];
    for ($i = 0; $i < count($xs); $i++) {
      $x = $xs[$i];
      if ($f($x)) {
        $yes[] = $x;
      } else {
        $no[] = $x;
      }
    }
    return [ 'yes' => yes, 'no' => no ];
  };
};

//------------------------------------------------------------------------------
// Sorting ---------------------------------------------------------------------
//------------------------------------------------------------------------------

$exports["sortImpl"] = function ($f) {
  return function ($l) use (&$f) {
    $l1 = array_merge($l);
    usort($l1, function($x, $y) use (&$f) {
      return $f($x)($y);
    });
    return $l1;
  };
};

//------------------------------------------------------------------------------
// Subarrays -------------------------------------------------------------------
//------------------------------------------------------------------------------

$exports["slice"] = function ($s) {
  return function ($e) use (&$s) {
    return function ($l) use (&$s, &$e) {
      return array_slice($l, $s, $e);
    };
  };
};

$exports["take"] = function ($n) {
  return function ($l) use (&$n) {
    return $n < 1 ? [] : array_slice($l, 0, $n);
  };
};

$exports["drop"] = function ($n) {
  return function ($l) use (&$n) {
    return $n < 1 ? $l : array_slice($l, $n);
  };
};

//------------------------------------------------------------------------------
// Zipping ---------------------------------------------------------------------
//------------------------------------------------------------------------------

$exports["zipWith"] = function ($f) {
  return function ($xs) use (&$f) {
    return function ($ys) use (&$f, &$xs) {
      $l = count($xs) < count($ys) ? count($xs) : count($ys);
      $result = [];
      for ($i = 0; $i < $l; $i++) {
        $result[$i] = $f($xs[$i])($ys[$i]);
      }
      return $result;
    };
  };
};

//------------------------------------------------------------------------------
// Partial ---------------------------------------------------------------------
//------------------------------------------------------------------------------

$exports["unsafeIndexImpl"] = function ($xs) {
  return function ($n) use (&$xs) {
    return $xs[$n];
  };
};
